/* Randomize array in-place using Durstenfeld shuffle algorithm */
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

class GameState {
  constructor () {
    const suits = ['hearts', 'clubs', 'diamonds', 'spades']

    this.deck = []
    suits.forEach(suit => {
      for (let i = 1; i <= 13; ++i) { this.deck.push({ suit: suit, number: i}) }
    })
    shuffleArray(this.deck)

    this.player = { score: 0, cards: [] }
    for (let n = 0; n < 5; ++n) { this.player.cards.push(this.deck.pop()) }

    this.computer = { score: 0, cards: [] }
    for (let n = 0; n < 5; ++n) { this.computer.cards.push(this.deck.pop()) }

    this.lastCard = null
    this.yourTurn = false
    this._computerTurn()
  }

  selectCard (cardIndex) {
    if (!this.yourTurn || !this._play(this.player, cardIndex)) { return }
    window.setTimeout(this._computerTurn.bind(this), 1000)
    this.yourTurn = false
    this.updateDisplay()
  }

  _computerTurn () {
    if (this.yourTurn || !this._play(this.computer, 0)) { return }
    this.yourTurn = true
    this.updateDisplay()
  }

  _play (whom, n) {
    if (n < 0 || n >= whom.cards.length) { return false }

    whom.score += whom.cards[n].number - (this.lastCard ? this.lastCard.number : 0)
    this.lastCard = whom.cards[n]
    whom.cards = whom.cards.filter((card, i) => i !== n)

    if (this.deck.length) { whom.cards.push(this.deck.pop()) }
    return true
  }

  updateDisplay () {
    const labels = { 1: 'A', 11: 'J', 12: 'Q', 13: 'K' }

    const table = document.querySelector('#last-card')
    table.innerHTML = ''

    if (this.lastCard) {
      const div = table.appendChild(document.createElement('div'))
      div.className= `${this.lastCard.suit} card`
      div.innerHTML = labels[this.lastCard.number] || String(this.lastCard.number)
      if (labels[this.lastCard.number]) { div.title = String(this.lastCard.number) }
    }

    const theirDeck = document.querySelector('#their-deck')
    theirDeck.innerHTML = ''

    this.computer.cards.forEach(() => {
      const div = theirDeck.appendChild(document.createElement('div'))
      div.className= 'card back'
    })

    const yourDeck = document.querySelector('#your-deck')
    yourDeck.innerHTML = ''

    this.player.cards.forEach((card, i) => {
      const button = yourDeck.appendChild(document.createElement('button'))
      button.className = `${card.suit} card`
      button.innerHTML = labels[card.number] || String(card.number)
      if (labels[card.number]) { button.title = String(card.number) }
      button.onclick = () => this.selectCard(i)
    })

    if (!this.player.cards.length && !this.computer.cards.length) {
      const gameOver = document.querySelector('#game-over')
      if (this.player.score > this.computer.score) {
        gameOver.innerHTML = 'You win!'
      } else if (this.player.score < this.computer.score) {
        gameOver.innerHTML = 'You lose...'
      } else {
        gameOver.innerHTML = 'Tie!'
      }
    }

    document.querySelector('#your-score').innerHTML = String(this.player.score)
    document.querySelector('#their-score').innerHTML = String(this.computer.score)
  }
}

window.onload = function () { new GameState() }
